Starting from a fresh react project, I cleared out some of the unecessary things that I wasn't going to use for the task. I also added the Assets, Components, Functions, and Pages folders so that I could organize the project from the start.

Assets could hold things like images, sounds, videos, or really any data that could be accessed many times. I elected to put my states and members types here so that if things ever needed to be added to, or reused elsewhere, it was easy to find and use. Components houses my heavily used tools, widgets, actions, etc. Things like buttons, inputs, tables, would be used ALL over the place. Having these standardized from the start will help in overall code and project maintainence. Functions is similar to Components, in that they can be used everywhere. In this case, I chose to make "my own" version of Axios so that it is factored to my liking for this project. This will also allow for easy swapping of Axios if there ever was a better package or function that would like to be used instead. Lastly Pages is where all my page views are stored. I threw in a couple extra folders to showcase why I elected to go with this folder structure.

Much like the server, I chose to go with pnpm as apposed to npm for its performate and efficiency reasons. Swapping to and from npm shouldn't be much of an issue. And either can be used.

Inside the actual page for Who's My Representative, it starts with View.jsx. I start every Page with View.jsx so that I can see the high level view of the page. Here, you can see a very simple layout of how the page works. You can then dive into the respective components to get more information as to what is going on.

I went ahead and started this project out with a state manager. I personally like Zustand for its lightweight and low boiler plate management. So, we have Store.jsx which sets up the store for my page, and houses my functions and variables, which I can then use all across my other components. Zustand allows me to easily move things around, and centralize my state in a much easier way. This prevents me from having to prop drill, or, try and use a more boiler platey Context.

For the styling, I went with the classic Bootstrap. This one is on version 5. I didn't go too crazy with the design. I just roughly followed what was shown. I did decide to do a few things a tad differently. So it definitely doesn't look exactly like the given image.

To start the frontend, we need to install the dependencies with `pnpm i`, then we can start with a `pnpm run start`.
