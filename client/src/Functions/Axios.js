import axios from 'axios';

export const Axios = ({ param = '', path = '' }) => {
    if (!param || !path) return { status: 422 };

    return axios.get(`http://localhost:3000/${path}/${param}`);
};
