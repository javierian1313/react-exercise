import { useMemo } from 'react';

export const Table = ({ columns = [], data = [] }) => {
    const headersMemo = useMemo(() => columns.map(({ name }, index) => <th key={index}>{name}</th>), [columns]);

    const dataMemo = useMemo(() => {
        const usedColumns = columns.reduce((prev, { key }) => [...prev, key], []);

        return data.map((row, index) => {
            return (
                <tr key={index}>
                    {columns.map(({ key = '', onClick = false }, index) => {
                        if (!usedColumns.includes(key)) return <></>;

                        return (
                            <td
                                className={typeof onClick === 'function' ? 'cursor-pointer link-primary' : ''}
                                key={index}
                                onClick={() => {
                                    if (typeof onClick === 'function') onClick({ row });
                                }}>
                                {row[key]}
                            </td>
                        );
                    })}
                </tr>
            );
        });
    }, [columns, data]);

    return (
        <div className='table-responsive'>
            <table className='table table-striped table-hover'>
                <thead>
                    <tr>{headersMemo}</tr>
                </thead>
                <tbody>{dataMemo}</tbody>
            </table>
        </div>
    );
};
