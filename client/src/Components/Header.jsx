export const Header = ({ color = 'dark', size = 3, text = '' }) => {
    return <div className={`fs-${size} text-${color}`}>{text}</div>;
};
