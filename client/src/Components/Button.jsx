export const Button = ({ color = 'success', disabled = false, onClick = () => {}, small = true, text = '' }) => {
    return (
        <div className='align-self-end mb-2'>
            <button className={`btn${small && ' btn-sm'} btn-${color}`} disabled={disabled} onClick={onClick}>
                {text}
            </button>
        </div>
    );
};
