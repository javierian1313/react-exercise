import { useMemo } from 'react';

export const Select = ({ onChange = () => {}, label = '', options = [], placeholder = '- Select -', small = true, value = '' }) => {
    const optionsMemo = useMemo(() => {
        return options.map(({ name, value }, index) => (
            <option key={index} value={value}>
                {name}
            </option>
        ));
    }, [options]);

    return (
        <div className='mb-2'>
            {label && <label className='form-label mb-0'>{label}</label>}
            <select className={`form-select${small && ' form-select-sm'}`} onChange={(e) => onChange(e.target.value)} value={value}>
                <option disabled value=''>
                    {placeholder}
                </option>
                {optionsMemo}
            </select>
        </div>
    );
};
