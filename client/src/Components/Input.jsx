export const Input = ({ disabled = false, onChange = () => {}, label = '', placeholder = '', small = true, value = '' }) => {
    return (
        <div className='mb-2'>
            {label && <label className='form-label mb-0'>{label}</label>}
            <input
                className={`form-control${small && ' form-control-sm'}`}
                disabled={disabled}
                onChange={(e) => onChange(e.target.value)}
                placeholder={placeholder}
                value={value}
            />
        </div>
    );
};
