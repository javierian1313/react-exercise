import { useWhosMyRepresentativeStore } from './Store';
import { members } from '../../Assets/Members';
import { states } from '../../Assets/States';
import { Button } from '../../Components/Button';
import { Select } from '../../Components/Select';

export const Selection = () => {
    const getMemberList = useWhosMyRepresentativeStore((state) => state.getMemberList);
    const selectedMembers = useWhosMyRepresentativeStore((state) => state.selectedMembers);
    const selectedState = useWhosMyRepresentativeStore((state) => state.selectedState);
    const setState = useWhosMyRepresentativeStore((state) => state.setState);

    return (
        <div className='d-flex gap-2 mt-5 mb-3'>
            <Select
                label='Type of Member'
                onChange={(e) => setState({ selectedMembers: e, viewInfo: false, viewList: false })}
                options={members}
                value={selectedMembers}
            />
            <Select label='State' onChange={(e) => setState({ selectedState: e, viewInfo: false, viewList: false })} options={states} value={selectedState} />
            <Button disabled={selectedMembers === '' || selectedState === ''} onClick={getMemberList} text='Submit' />
        </div>
    );
};
