import { Header } from '../../Components/Header';
import { ListAndInfo } from './ListAndInfo';
import { Selection } from './Selection';

export const WhosMyRepresentative = () => {
    return (
        <>
            <Header color='primary' size={2} text={`Who's My Representative?`} />
            <Selection />
            <hr />
            <ListAndInfo />
        </>
    );
};
