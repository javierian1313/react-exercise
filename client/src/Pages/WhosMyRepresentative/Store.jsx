import create from 'zustand';
import { Axios } from '../../Functions/Axios';

export const useWhosMyRepresentativeStore = create((set, get) => ({
    getMemberList: async () => {
        const { selectedMembers, selectedState } = get();

        const { data, status } = await Axios({ param: selectedState, path: selectedMembers });

        if (status === 422) return alert(`Seems like you do not have all the data, let's try that one more time.`);
        if (status !== 200) return alert('Uh oh. Looks like something went wrong. Please reach out to dev team.');

        set({ membersList: await data.results, viewList: true });
    },
    membersList: [],
    membersListColumns: [
        { key: 'name', name: 'Name', onClick: ({ row }) => get().showInfo({ row }) },
        { key: 'party', name: 'Party' },
    ],
    selectedMembers: '',
    selectedState: '',
    setState: (object) => set(object),
    showInfo: ({ row: { district, link, name, office, party, phone, state } }) => set({ district, link, name, office, party, phone, state, viewInfo: true }),
    viewInfo: false,
    viewList: false,
}));
