import { Header } from '../../Components/Header';
import { Input } from '../../Components/Input';
import { Table } from '../../Components/Table';
import { useWhosMyRepresentativeStore } from './Store';

export const ListAndInfo = () => {
    const district = useWhosMyRepresentativeStore((state) => state.district);
    const link = useWhosMyRepresentativeStore((state) => state.link);
    const membersList = useWhosMyRepresentativeStore((state) => state.membersList);
    const membersListColumns = useWhosMyRepresentativeStore((state) => state.membersListColumns);
    const name = useWhosMyRepresentativeStore((state) => state.name);
    const office = useWhosMyRepresentativeStore((state) => state.office);
    const party = useWhosMyRepresentativeStore((state) => state.party);
    const phone = useWhosMyRepresentativeStore((state) => state.phone);
    const selectedMembers = useWhosMyRepresentativeStore((state) => state.selectedMembers);
    const state = useWhosMyRepresentativeStore((state) => state.state);
    const viewInfo = useWhosMyRepresentativeStore((state) => state.viewInfo);
    const viewList = useWhosMyRepresentativeStore((state) => state.viewList);

    return (
        <div className='row'>
            <div className={viewInfo ? 'col-6' : 'col-12'}>
                {viewList && <Header text={`List / ${selectedMembers}`} />}
                {viewList && <Table columns={membersListColumns} data={membersList} />}
            </div>
            <div className='col-6'>
                {viewInfo && <Header text='Info' />}
                {viewInfo && (
                    <div className='mt-2'>
                        {name !== '' && <Input disabled label='Name' value={name} />}
                        {party !== '' && <Input disabled label='Party' value={party} />}
                        {state !== '' && <Input disabled label='State' value={state} />}
                        {district !== '' && <Input disabled label='District' value={district} />}
                        {office !== '' && <Input disabled label='Office' value={office} />}
                        {link !== '' && <Input disabled label='Link' value={link} />}
                        {phone !== '' && <Input disabled label='Phone' value={phone} />}
                    </div>
                )}
            </div>
        </div>
    );
};
