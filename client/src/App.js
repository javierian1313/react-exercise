import './App.css';
import { WhosMyRepresentative } from './Pages/WhosMyRepresentative/View';

export const App = () => {
    return (
        <div className='container'>
            <div className='row'>
                <WhosMyRepresentative />
            </div>
        </div>
    );
};
