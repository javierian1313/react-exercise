With the server provided, there really wasn't much work to be done here. Most, if not all, has remained untouched.

I chose to organize this into a server folder so that it is completely separate from the frontend, or the client.

I had to adjust the package.json so that it knew where my index.js file was located.

nodemon was also installed so that I could get the auto restart for my server.

pnpm was used instead of npm. It is supposed to be much more performate and efficient than npm is. So, I elected to go with it and made the server have its own node_modules independant from the client. Just in case things needed to be separated. This could be a good reason so that we could Dockerize client and server separate from one another.

Overall, not much to say on the server end.

To start the server, we will need to install dependencies with a `pnpm i` and then followed by either `nodemon` or `pnpm run start` to run the server.
